<?php


return [
	// Client ID của app mà bạn đã đăng ký trên PayPal Dev
    'client_id' => env('PAYPAL_CLIENT_ID'),
    // Secret của app
    'secret' => env('PAYPAL_SECRET'),
    'settings' => [
    	// PayPal mode, sanbox hoặc live
        'mode' => env('PAYPAL_MODE'),
        // Thời gian của một kết nối (tính bằng giây)
        'http.ConnectionTimeOut' => 30,
        // Có ghi log khi xảy ra lỗi
        'log.logEnabled' => true,
        // Đường dẫn đền file sẽ ghi log
        'log.FileName' => storage_path() . '/logs/paypal.log',
        // Kiểu log
        'log.LogLevel' => 'FINE'
    ],
];

// return [

//     'client_id'=> 'AVMQ5czrwU91PmyH8P6r_zm7OyJqWNbQgExVxpDUYjlstwdDbwWOWtu9F7iA_at_P4br9r7CH2wPYmST',
//     'secret' => 'EMBOa2zvRiV3p1Yl_kBguMa0W4dbiKEqPsrzebRqGLpCJHvXT5b9duxDI5YiM2yvhmnFMhBNkDeoLshT',
//     'settings' => [
//         'mode' => 'sanbox',
//         'http.CURLOPT_CONNECTIONTIMEOUT' => 30,
//         'log.logEnabled' => true,
//         'log.FileName' => storage_path() . '/logs/paypal.log',
//         'log.Loglevel' => 'FINE'
//     ],
// ];
// ?>