<?php

namespace App\Http\Controllers;

use Request;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Config;
use URL,Session,Redirect;


class PaymentController extends Controller
{
    private $_api_context;

    public function __construct()
    {
      
        // setup PayPal api context
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function postPayment()
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        // Items vi du
        $item_1 = new Item();
        $item_1->setName('Item 1') // item name
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice('3'); // unit price

        // $item_2 = new Item();
        // $item_2->setName('Item 2')
        //     ->setCurrency('EUR')
        //     ->setQuantity(1)
        //     ->setPrice('1');

        // $item_3 = new Item();
        // $item_3->setName('Item 3')
        //     ->setCurrency('EUR')
        //     ->setQuantity(1)
        //     ->setPrice('20');

        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal(3);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
            ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // redirect to paypal
            return Redirect::away($redirect_url);
        }

        return Redirect::route('original.route')
            ->with('error', 'Unknown error occurred');
    }
    public function getPaymentStatus()
    {
    	// Khởi tạo request để lấy một số query trên
        // URL trả về từ PayPal
        $request = Request::all();

        // Lấy Payment ID từ session
        $paymentId = session('paypal_payment_id');
        // Xóa payment ID đã lưu trong session
        session()->forget('paypal_payment_id');

        // Kiểm tra xem URL trả về từ PayPal có chứa
        // các query cần thiết của một thanh toán thành công
        // hay không.
        if (empty($request['PayerID']) || empty($request['token'])) {
            return false;
        }

        // Khởi tạo payment từ Payment ID đã có
        $payment = Payment::get($paymentId, $this->_api_context);

        // Thực thi payment và lấy payment detail
        $paymentExecution = new PaymentExecution();
        $paymentExecution->setPayerId($request['PayerID']);

        $paymentStatus = $payment->execute($paymentExecution, $this->_api_context);

        // return $paymentStatus;

         dd($paymentStatus);

        // $limit = 10;
        // $offset = 0;

        // $paymentList = $this->getPaymentList($limit, $offset);

        // dd($paymentList);

        // $paymentDetails = $this->getPaymentDetails($paymentId);

        // dd($paymentDetails);

        // $data2 = json_decode($paymentStatus, true);
        // $value['fee'] = $data2['payer']['status'];
        // dd($value);

       
    }

    public function getPaymentList($limit = 10, $offset = 0)
    {
        $params = [
            'count' => $limit,
            'start_index' => $offset
        ];

        try {
            $payments = Payment::all($params, $this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $paypalException) {
            throw new \Exception($paypalException->getMessage());
        }

        return $payments;
    }

    public function getPaymentDetails($paymentId)
    {
        try {
            $paymentDetails = Payment::get($paymentId, $this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $paypalException) {
            throw new \Exception($paypalException->getMessage());
        }

        return $paymentDetails;
    }

    public function OutputPay()
    {
        $result = json_decode($paymentStatus, true);
        $request = $result['links'][0]['href']; //get link
        // $client2 = 
        // $response2 = $client2->request('GET', $request, [
        //     'headers' =>
        //     [
        //         'Authorization' => 'Bearer' . $access_token, //access token
        //         'Content-Type' =>'application/json',    
        //     ],
        // ]);
        $data2 = $paymentStatus;
        $value['fee'] = $data2['batch_header']['fees']['value'];
        $value['money'] = $data2['batch_header']['amount']['value'];
        $value['total'] = $data2['batch_header']['fees']['value'] + $data2['batch_header']['amount']['value'];
        $value['type'] = $data2['batch_header']['amount']['currency'];
        return view('notify',$value);
    }

}
